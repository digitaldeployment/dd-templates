# Digital Deployment Base Styling Engine

A less library which serves as a starting point for all project themes. This library allows developers to hand pick any/all common component styling needed for a typical theme. It also offers the flexibility to substitute out a component for a replacement while preserving the original cascade of styling.
